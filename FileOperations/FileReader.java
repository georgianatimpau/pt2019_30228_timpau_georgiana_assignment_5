package FileOperations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import Business.MonitoredData;

public class FileReader {
	
	public static List<MonitoredData> readFile() {

		String file = "Activities.txt";

		try (Stream<String> stream = Files.lines(Paths.get(file))) {
			List<MonitoredData> list = stream.map(line -> line.split("\t\t"))
					.map(a -> new MonitoredData(a[0], a[1], a[2])).collect(Collectors.toList());

			return list;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
