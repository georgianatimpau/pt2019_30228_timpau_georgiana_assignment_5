package FileOperations;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import Business.MonitoredData;

public class FileWriter {

	public void pritnt1(Map<String, Integer> map) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Cerinta 3.pdf"));
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}
		document.open();
		try {
			document.add(new Paragraph("Counting activity over the entire monitoring period"));

			for (String activity : map.keySet()) {
				document.add(new Paragraph(activity + " appears " + map.get(activity) + " times"));
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}

	public void pritnt2(Map<Integer, Map<String, Integer>> map) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Cerinta 4.pdf"));
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}
		document.open();
		try {
			document.add(new Paragraph(
					"How many times has appeared each activity for each day over the monitoring period\n\n"));

			for (Integer activity : map.keySet()) {
				document.add(new Paragraph(activity + " \n " + map.get(activity) + "\n"));
			}

		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	public void pritnt3(Map<MonitoredData, Long> map) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Cerinta 5.pdf"));
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}
		document.open();
		try {
			document.add(new Paragraph(
					"For each line from the file map for the activity label the duration recorded on that line\n\n"));

			for (MonitoredData activity : map.keySet()) {
				document.add(new Paragraph(activity + " duration " + map.get(activity) + "minutes\n"));
			}

		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	public void pritnt4(Map<String, Long> map) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Cerinta 6.pdf"));
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}
		document.open();
		try {
			document.add(new Paragraph(
					"For each activity compute the entire duration over the monitoring period\n\n"));

			for (String activity : map.keySet()) {
				document.add(new Paragraph(activity + " duration " + map.get(activity) + "\n"));
			}

		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	public void pritnt5(Map<String, Integer> map) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Cerinta 7.pdf"));
		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		}
		document.open();
		try {
			document.add(new Paragraph(
					"Filter the activities that have 90% of the monitoring records with duration less than 5\r\n" + 
					"minutes\r\n" + 
					"\n\n"));

			for (String activity : map.keySet()) {
				document.add(new Paragraph(activity + "\n"));
			}

		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
}