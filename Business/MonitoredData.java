package Business;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {

	String start_time;
	String end_time;
	String activity;

	public MonitoredData() {

	}

	public MonitoredData(String start_time, String end_time, String activity) {
		this.start_time = start_time;
		this.end_time = end_time;
		this.activity = activity;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public long computeDuration() {
		
		return Duration.between(DataController.parseTime(getStart_time()), DataController.parseTime(getEnd_time())).toMinutes();
	}
	
	public int getStartDateDayOfTheMonth() {
		return DataController.parseDate(start_time).getDayOfMonth();
	}

	@Override
	public String toString() {
		return activity + "  " + start_time + "  " + end_time;
	}

	
}
