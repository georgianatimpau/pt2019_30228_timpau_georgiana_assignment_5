package Business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataController {
	
	public static long countDays(List<MonitoredData> activities) {

		return activities.stream().map(a -> DataController.parseDate(a.getStart_time()).getDayOfMonth()).distinct().count();

	}
	
	public static LocalDate parseDate(String string) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date date = simpleDateFormat.parse(string);
			return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static LocalDateTime parseTime(String string) {
		try {
			String replace = string.replace(' ', 'T');
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			return LocalDateTime.parse(replace);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static HashMap<String, Integer> activitiesOverMonitoring(List<MonitoredData> activities) {

		HashMap<String, Integer> map = new HashMap<String, Integer>();

		activities.stream().forEach(m -> {
			if (map.get(m.activity) != null)
				map.put(m.activity, map.get(m.activity) + 1);
			else
				map.put(m.activity, 1);
		});

		return map;
	}

	public static Map<Integer, Map<String, Integer>> countActivitiesDay(List<MonitoredData> activities) {

		Map<Integer, Map<String, Integer>> mapActivitiesForEachDay;
		mapActivitiesForEachDay = activities.stream().collect(Collectors.groupingBy(MonitoredData::getStartDateDayOfTheMonth,
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingInt(x -> 1))));

		return mapActivitiesForEachDay;
	}

	public static Map<MonitoredData, Long> durationActivitieLine(List<MonitoredData> activities) {

		Map<MonitoredData, Long> duration = new HashMap<MonitoredData, Long>();

		activities.stream().forEach(a -> duration.put(a, a.computeDuration()));

		return duration;
	}

	public static Map<String, Long> durationActivitieTotal(List<MonitoredData> activities) {

		Map<String, Long> duration = new HashMap<String, Long>();

		duration = activities.stream().collect(
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingLong(x -> x.computeDuration()) ));
		
		duration.entrySet().stream().forEach(m -> m.setValue( m.getValue()/3600 ) );

		return duration;
	}

	public static Map<String, Integer> filter(List<MonitoredData> activities) {

		Map<String, Integer> activities_less5min;

		Map <String, Integer> groupByActivitie = DataController.activitiesOverMonitoring(activities);
		
		activities_less5min = activities.stream().filter(m -> m.computeDuration() < 5).
				collect( Collectors.groupingBy(MonitoredData :: getActivity, Collectors.summingInt(x -> 1)) );
		
		activities_less5min.entrySet().removeIf( m-> m.getValue() < 0.9 * groupByActivitie.get( m.getKey() ));

		return activities_less5min;
	}
}
