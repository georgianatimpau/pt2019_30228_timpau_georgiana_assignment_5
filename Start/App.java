package Start;

import java.util.List;

import javax.xml.datatype.Duration;

import Business.DataController;
import Business.MonitoredData;
import FileOperations.FileReader;
import FileOperations.FileWriter;

public class App {
	public static void main(String[] args) {
		
		List<MonitoredData> activities = new FileReader().readFile();

		System.out.println("In the log appear " + DataController.countDays(activities) + " days \n");
		
		FileWriter f  =  new FileWriter();
		
		f.pritnt1( DataController.activitiesOverMonitoring(activities)  );
		
		f.pritnt2( DataController.countActivitiesDay(activities) );
		
		f.pritnt3( DataController.durationActivitieLine(activities) );
		
		f.pritnt4( DataController.durationActivitieTotal(activities) );
		
		f.pritnt5( DataController.filter(activities) );

	} 
}
